<?php

/**
 * @file
 * The Image+ module page.
 */

/**
 * @defgroup image_plus Image+
 * @ingroup drupal_plus
 * @{
 * Extending drupal solutions.
 */
/* SYSTEM HOOKS */

function image_plus_theme() {
  $variables = \Drupal\image_plus\Plugin\Field\FieldFormatter\ImagePlusFormatter::defaultSettings();

  return array(
    'image_plus' => array(
      'variables' => $variables,
      'template' => 'image_plus'
    )
  );
}

/* IMAGE_PLUS HOOKS */

/**
 * Implements hook_image_plus_settings().
 */
function image_plus_image_plus_settings() {
  return array(
    'style' => FALSE,
    'link' => FALSE,
  );
}

/**
 * Implements hook_image_plus_settings_form().
 */
function image_plus_image_plus_settings_form($settings) {
  $element = array();

  $image_styles = image_style_options(FALSE);
  $element['style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#default_value' => $settings['style'],
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
    '#tree' => TRUE,
  );

  $link_types = array(
    'content' => t('Content'),
    'file' => t('File'),
  );
  $element['link'] = array(
    '#title' => t('Link image to'),
    '#type' => 'select',
    '#default_value' => $settings['link'],
    '#empty_option' => t('Nothing'),
    '#tree' => TRUE,
    '#options' => $link_types,
  );

  return $element;
}

function image_plus_image_plus_settings_summary($settings) {
  $summary = array();

  $image_styles = image_style_options(FALSE);
  // Unset possible 'No defined styles' option.
  unset($image_styles['']);
  // Styles could be lost because of enabled/disabled modules that defines
  // their styles in code.
  $image_style_setting = $settings['style'];
  if (isset($image_styles[$image_style_setting])) {
    $summary[] = t('Image style: @style',
        array('@style' => $image_styles[$image_style_setting]));
  }
  else {
    $summary[] = t('Original image');
  }

  $link_types = array(
    'content' => t('Linked to content'),
    'file' => t('Linked to file'),
  );
  // Display this setting only if image is linked.
  $image_link_setting = $settings['link'];
  if (isset($link_types[$image_link_setting])) {
    $summary[] = $link_types[$image_link_setting];
  }

  return $summary;
}

/**
 * Implements hook_image_plus_prerender().
 */
function image_plus_image_plus_prerender($elements, $items,
    $formatter) {
  $settings = $formatter->getSetting('image_plus');

  $default_link_options = array(
    'attributes' => array('class' => array('image-plus-link', 'link'))
  );

  $image_link_setting = $settings['values']['link'];
  // Check if the formatter involves a link.
  if ($image_link_setting == 'content') {
    $entity = $items->getEntity();
    if (!$entity->isNew()) {
      // @todo Remove when theme_image_plus_formatter() has support for route name.
      $uri['path'] = $entity->getSystemPath();
      $uri['options'] = $entity->urlInfo()->getOptions() + $default_link_options;
    }
  }
  elseif ($image_link_setting == 'file') {
    $link_file = TRUE;
  }

  $style_name = $settings['values']['style'];

  // Collect cache tags to be added for each item in the field.
  $cache_tags = array();
  if (!empty($style_name)) {
    $image_style = entity_load('image_style', $style_name);
    $cache_tags = $image_style->getCacheTag();
  }

  foreach ($items as $delta => $item) {
    if ($item->entity) {
      if (isset($link_file)) {
        $image_uri = $item->entity->getFileUri();
        $uri = array(
          'path' => file_create_url($image_uri),
          'options' => $default_link_options,
        );
      }

      if (isset($image_style)) {
        $elements[$delta]['#image_plus']['style'] = FALSE;
        $elements[$delta]['#image']['#theme'] = 'image_style';
        $elements[$delta]['#image']['#style_name'] = $style_name;
        $elements[$delta]['#image']['#cache'] = array('tags' => $cache_tags);
      }

      if (isset($uri)) {
        $elements[$delta]['#image_plus']['link'] = $uri;
      }
    }
  }
  return $elements;
}

/* HELPER FUNCTIONS */

function template_preprocess_image_plus(&$variables) {
  unset($variables['title_attributes']);
  unset($variables['content_attributes']);
  unset($variables['title_prefix']);
  unset($variables['title_suffix']);

  $variables['attributes'] += array(
    'class' => array('image-plus-wrapper')
  );

  if ($variables['image_plus']['link']) {
    $link = $variables['image_plus']['link'];
    $link['options'] += array('attributes' => array());
    $link['options']['attributes'] = array_merge_recursive($link['options']['attributes'],
        array('href' => url($link['path'], $link['options'])));
    $variables['attributes'] = array_merge_recursive($variables['attributes'],
        $link['options']['attributes']);
  }

  $variables['attributes'] = new \Drupal\Core\Template\Attribute($variables['attributes']);
}

function _image_plus_compare_modules($a, $b) {
    if ($a->sort == $b->sort) {
        return 0;
    }
    return ($a->sort > $b->sort) ? -1 : 1;
}

/**
 * @} End of "defgroup image_plus".
 */