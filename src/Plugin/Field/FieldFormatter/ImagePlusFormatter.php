<?php

/**
 * @file
 * Contains \Drupal\image_plus\Plugin\Field\FieldFormatter\ImagePlusFormatter
 */
/**
 * @addtogroup image_plus
 * @{
 */

namespace Drupal\image_plus\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Extension\InfoParser;
use Kint;

/**
 * Plugin implementation of the 'image' formatter.
 *
 * @FieldFormatter(
 *   id = "image_plus",
 *   label = @Translation("Image+"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ImagePlusFormatter extends ImageFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = array('image_object' => NULL, 'image' => array());

    $hook = 'image_plus_settings';
    $implementations = \Drupal::moduleHandler()->getImplementations($hook);
    foreach ($implementations as $module) {
      $settings += array(
        $module => array(
          'enabled' => FALSE,
          'values' => \Drupal::moduleHandler()->invoke($module, $hook),
        )
      );
    }

    return $settings + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = array(
      '#type' => 'fieldset',
      '#attributes' => array('class' => array('field-display-image-plus')),
      '#attached' => array('library' => array('image_plus/field-display'))
    );

    $field_name = $this->fieldDefinition->getName();

    $hook = 'image_plus_settings_form';
    $implementations = \Drupal::moduleHandler()->getImplementations($hook);
    foreach ($implementations as $module) {
      $object = \Drupal::moduleHandler()->getModule($module);
      $info = $object->serialize();
      $name = !empty($info['name']) ? $info['name'] : $object->getName();
      $settings = $this->getSetting($module);

      $element[$module] = array(
        '#type' => 'fieldset',
        '#tree' => true,
      );
      $element[$module]['enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t($name),
        '#tree' => TRUE,
        '#default_value' => $settings['enabled']
      );
      $element[$module]['values'] = array(
        '#type' => 'fieldset',
        '#tree' => true,
        '#collapsed' => FALSE,
        '#collapsible' => FALSE,
        '#states' => array(
          // Only show this field when the 'toggle_me' checkbox is enabled.
          'enabled' => array(
            ":input[name='fields[$field_name][settings_edit_form]"
            . "[settings][$module][enable]']" => array('checked' => TRUE),
          ),
          'required' => array(
            ":input[name='fields[$field_name][settings_edit_form]"
            . "[settings][$module][enable]']" => array(
              'checked' => TRUE),
          ),
        ),
      );

      $element[$module]['values'] += \Drupal::moduleHandler()
          ->invoke($module, 'image_plus_settings_form',
          array($settings['values']));
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();

    $hook = 'image_plus_settings_summary';
    $implementations = \Drupal::moduleHandler()->getImplementations($hook);
    foreach ($implementations as $module) {
      $object = \Drupal::moduleHandler()->getModule($module);
      $info = $object->serialize();
      $name = !empty($info['name']) ? $info['name'] : $object->getName();
      $settings = $this->getSetting($module);

      if ($settings['enabled']) {
        $summary[] = t('<b>@name</b>', array('@name' => $name));
        $module_summary = \Drupal::moduleHandler()->invoke($module, $hook,
            array($settings['values']));
        $summary = array_merge($summary, $module_summary);
      }
    }

    if (empty($summary)) {
      return array(t('Original Image'));
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $elements = array();

    foreach ($items as $delta => $item) {
      if ($item->entity) {

        // Extract field item attributes for the theme function, and unset them
        // from the $item so that the field template does not re-render them.
        $item_attributes = $item->_attributes;
        unset($item->_attributes);

        $elements[$delta] = array(
          '#theme' => 'image_plus',
          '#image_object' => $item,
          '#image' => array(
            '#theme' => 'image',
            '#attributes' => $item_attributes,
            '#uri' => $item->entity->getFileUri()
          ),
        );

        foreach (array('width', 'height', 'alt', 'title') as $name) {
          if (!empty($item->$name)) {
            $elements[$delta]['#image']["#$name"] = $item->$name;
          }
        }
      }
    }

    $parser = new InfoParser();

    $modules_to_sort = array();

    $hook = 'image_plus_prerender';
    $module_names = \Drupal::moduleHandler()->getImplementations($hook);
    foreach ($module_names as $module_name) {
      $modules_to_sort[$module_name] = \Drupal::moduleHandler()->getModule($module_name);
      $modules_to_sort[$module_name]->info = $parser->parse($modules_to_sort[$module_name]->getPathname());
    }

    $modules = \Drupal::moduleHandler()->buildModuleDependencies($modules_to_sort);

    \Drupal::moduleHandler()->load('image_plus');
    usort($modules, '_image_plus_compare_modules');

    kint_require();
    for ($i = 0; $i < count($modules); $i++) {
      $module_name = $modules[$i]->getName();
      $return = \Drupal::moduleHandler()->invoke($module_name, $hook,
          array($elements, $items, $this));
//      $result = array();
//      foreach ($elements as $delta => $element) {
//        $result[$delta] = NestedArray::mergeDeep($return[$delta], $element);
//      }
      $elements = NestedArray::mergeDeepArray(array($return, $elements), TRUE);
    }

    return $elements;
    //\Drupal::moduleHandler()
    //      ->invokeAll('image_plus_prerender', array($elements, $items, $this));
  }

}

/**
   * @}
   */
  