(function($, Drupal) {

  Drupal.behaviors.fuzzyLoadAdmin = {
    attach: function(context/*, settings*/) {
      $('#fuzzy-load-admin #edit-levels', context).once(function() {
        var current = $(this).data('current');
        $('> .fieldset-wrapper > fieldset', this).each(function(index) {
          var $rangeField = $('.form-item-levels-' + index + '-level', this);
          var $rangeLabel = $('label', $rangeField);
          
          $('input[type=range]', $rangeField).on('input', function(e) {
            $rangeLabel.text(Drupal.t(current, {'@level': this.value}));
          });
        });
      });
    }
  };

})(jQuery, Drupal);