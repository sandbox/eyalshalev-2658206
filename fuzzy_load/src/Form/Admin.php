<?php

/**
 * @file
 * Contains \Drupal\fuzzy_load\Form\FuzzyLoadForm.
 */

namespace Drupal\fuzzy_load\Form;

use \Drupal\Core\Form\ConfigFormBase;
use \Drupal\Core\Form\FormStateInterface;
use Kint;

/**
 * Fuzzy load form class
 */
class Admin extends ConfigFormBase {

  public function getFormId() {
    // Unique ID of the form.
    return 'fuzzy_load_admin';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $levels = $this->config('fuzzy_load.settings')->get('levels');

    $form['#attached'] = array(
      'library' => array(
        'fuzzy_load/admin-page'
      ),
    );

    $form['levels'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Fuzzy Load level'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
      '#attributes' => array(
        'data-current' => 'Current value: @level%',
      ),
    );

    for ($i = 0; $i <= count($levels); $i++) {
      $level = isset($levels[$i]) ? $levels[$i] : NULL;

      $form['levels'][$i] = array(
        '#type' => 'fieldset',
        '#title' => $this->t('Saved value: @level%', array('@level' => $level)),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        '#tree' => TRUE,
      );
      $form['levels'][$i]['enable'] = array(
        '#type' => 'checkbox',
        '#tree' => TRUE,
        '#default_value' => TRUE
      );
      $form['levels'][$i]['level'] = array(
        '#type' => 'range',
        '#tree' => TRUE,
        '#title' => ' ',//$this->t('Current value: @level%', array('@level' => $level)),
        '#default_value' => $level,
        '#min' => 0, '#max' => 100, '#step' => 5,
        '#states' => array(
          // Only show this field when the 'toggle_me' checkbox is enabled.
          'enabled' => array(
            ":input[name='levels[$i][enable]']" => array('checked' => TRUE),
          ),
        )
      );

      if (is_null($level)) {
        $form['levels'][$i]['#title'] = $this->t('Move this to set a new level.');
        $form['levels'][$i]['enable']['#default_value'] = FALSE;
      }
    }

    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state['values']['levels'] as $fieldset) {
      if (!empty($fieldset['level']) && $fieldset['enable']) {
        $level = intval($fieldset['level']);
        $levels[$level] = $level;
      }
    }
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $levels = array();

    foreach ($form_state['values']['levels'] as $fieldset) {
      if (!empty($fieldset['level']) && $fieldset['enable']) {
        $level = intval($fieldset['level']);
        $levels[$level] = $level;
      }
    }

    $newLevels = array_values($levels);
    sort($newLevels);

    $this->config('fuzzy_load.settings')
        ->set('levels', $newLevels)
        ->save();
    parent::submitForm($form, $form_state);
  }

}
