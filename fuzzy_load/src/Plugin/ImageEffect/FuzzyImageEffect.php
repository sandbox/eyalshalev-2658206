<?php

/**
 * @file
 * Contains \Drupal\fuzzy_load\Plugin\ImageEffect\FuzzyImageEffect
 */

namespace Drupal\fuzzy_load\Plugin\ImageEffect;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageInterface;
use Drupal\image\ConfigurableImageEffectBase;

/**
 * Fuzzies an image resource.
 *
 * @ImageEffect(
 *   id = "fuzzy",
 *   label = @Translation("Fuzzy"),
 *   description = @Translation("Fuzzy will make images become of lower quality.")
 * )
 */
class FuzzyImageEffect extends ConfigurableImageEffectBase {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {

    $fuzzy_level = $this->configuration['fuzzy_level'];
    $quality = (100 - $fuzzy_level) / 100;

    $width = $image->getWidth();
    $height = $image->getHeight();
    $fuzzy_width = $width * $quality;
    $fuzzy_height = $height * $quality;

    if (!$image->resize($fuzzy_width, $fuzzy_height)) {
      $this->logger->error('Image resize failed using the %toolkit toolkit on %path (%mimetype, %dimensions)',
          array('%toolkit' => $image->getToolkitId(), '%path' => $image->getSource(),
        '%mimetype' => $image->getMimeType(), '%dimensions' => $image->getWidth() . 'x' . $image->getHeight()));
      return FALSE;
    }
    
    if (!$image->scale($width, $height, true)) {
      $this->logger->error('Image resize failed using the %toolkit toolkit on %path (%mimetype, %dimensions)',
          array('%toolkit' => $image->getToolkitId(), '%path' => $image->getSource(),
        '%mimetype' => $image->getMimeType(), '%dimensions' => $image->getWidth() . 'x' . $image->getHeight()));
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function transformDimensions(array &$dimensions) {
    
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = array(
      '#theme' => 'fuzzy_load_summary',
      '#data' => $this->configuration,
    );
    $summary += parent::getSummary();

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'fuzzy_level' => NULL,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form,
      FormStateInterface $form_state) {
    $levels = \Drupal::config('fuzzy_load.settings')->get('levels');

    $options = array();
    foreach ($levels as $level) {
      $options[$level] = $this->t('@level% fuzzy', array('@level' => $level));
    }

    $form['fuzzy_level'] = array(
      '#type' => 'select',
      '#title' => t('Fuzzy Level'),
      '#default_value' => $this->configuration['fuzzy_level'],
      '#options' => $options,
      '#required' => TRUE,
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form,
      FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['fuzzy_level'] = $form_state['values']['fuzzy_level'];
  }

}
